use serde_derive::{Deserialize, Serialize};

#[derive(Serialize)]
pub struct VersionResponse {
    pub version: String,
}

impl VersionResponse {
    pub fn of(ver: String) -> VersionResponse {
        VersionResponse {
            version: ver
        }
    }
}

