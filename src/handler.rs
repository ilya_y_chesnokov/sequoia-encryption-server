use warp::{http::StatusCode, reply::json, Reply};
use sequoia_openpgp as openpgp;
use crate::{data}

mod handler {
    pub async fn version_handler() -> Result<impl Reply> {
        Ok(json(&VersionResponse::of(
            openpgp::VERSION.into()
        }
    }  
}