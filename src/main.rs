use handler

use warp::{Filter, Rejection};
use sequoia_openpgp as openpgp;

mod data;
mod handler;
mod error;

type Result<T> = std::result::Result<T, Rejection>;

#[tokio::main]
async fn main() {
    // GET /hello/warp => 200 OK with body "Hello, warp!"
    let health_route = warp::path!("health")
        .map(|| StatusCode::OK);

    let version = warp::path!("version").and_then(handler::version_handler);

    let routes = health_route.or(version)
        .with(warp::cors().allow_any_origin())
        .recover(error::handle_rejection);

    

    warp::serve(routes)
        .run(([127, 0, 0, 1], 3030))
        .await;
}
